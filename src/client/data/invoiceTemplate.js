export default {
  id: 1,
  executive: 'EXECUTIVE Shankar label agency,Jaipur,India, ZIP: XXX-XX-XX-XXX',
  recipient: 'RECIPIENT: Dux publication,Delhi,India ZIP: XXX-XX-XX-XXX',
  invoiceTitle: 'Template',
  invoiceDate: 'Place and date: 12.12.1989',
  labels: {
    nrLabel: 'Nr.',
    serviceNameLabel: 'Service name',
    amountLabel: 'Amt',
    priceNettoLabel: 'Unit/Quantity',
    valueNettoLabel: 'Value',
    valRateLabel: 'VAT',
    valValueLabel: 'VAT amount',
    vatRateValue: '-',
    fullValueLabel: 'Value'
  },
  services: [
    {
      id: 1,
      name: '',
      amount: '',
      Quantity: '',
      vat: ''
    }
  ],
  valueInWords: 'In word: hundreds of millions',
  paymentType: 'Payment type: account transfer',
  accountNumber: 'Account number: (mbank) XX XXXX XXXX XXXX XXXX XXXX XXXX'
}
